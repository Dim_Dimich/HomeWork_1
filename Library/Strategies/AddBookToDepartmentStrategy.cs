﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class AddBookToDepartmentStrategy : IStrategy
    {
        public void DoSomething(Library library)
        {
            Console.Clear();
            Department departmentForBook = library.ChoiceDepartment();
            library.AddBookToDepartment(departmentForBook);
        }
    }
}
