﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class SearchBookByNameStartegy : IStrategy
    {
        public void DoSomething(Library library)
        {
            string searchName = "";
            Console.Clear();
            Console.WriteLine("Enter book name : \n");
            searchName = Console.ReadLine();
            library.SearchBookByName(searchName);
        }
    }
}
