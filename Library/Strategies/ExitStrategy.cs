﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class ExitStrategy : IStrategy
    {
        public void DoSomething(Library library)
        {
            Console.WriteLine("\nGood luck\n)");
            var write = new WorkWithXml();
            write.WriteInXmlDocument(library);
        }
    }
}
