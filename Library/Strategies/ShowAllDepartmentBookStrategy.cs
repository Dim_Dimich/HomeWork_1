﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Library
{
    class ShowAllDepartmentBookStrategy : IStrategy
    {
        public void DoSomething(Library library)
        {
            Console.Clear();
            Department department = library.ChoiceDepartment();
            department.ShowAllBookInDepartment();
        }
    }
}
