﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class ShowAutorWithTheMostBooksStrategy : IStrategy
    {
        public void DoSomething(Library library)
        {
            Console.Clear();
            library.ShowAutorWithTheMostBooks();
        }
    }
}
