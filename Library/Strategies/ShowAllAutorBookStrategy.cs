﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class ShowAllAutorBookStrategy : IStrategy
    {
        public void DoSomething(Library library)
        {
            Console.Clear();
            Autor autor = library.ChoiceAutor();
            autor.ShowAllBooksForAutor();
        }
    }
}
