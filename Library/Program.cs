﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Program
    {
        public static IStrategy menuStrategy = new DefaultStrategy();
        //public void ChangeStartegy(IStrategy menuStrategy)
        //{
        //    _menuStrategy = menuStrategy;
        //}
        static void Main(string[] args)
        {
            int choice = -1;
            var library = new Library();
            AddAutorsToProgram(library);
            var read = new WorkWithXml();
            read.ReadFromXmlDocument(library);
            Dictionary<int, IStrategy> strategies = new Dictionary<int, IStrategy>(11);
            strategies.Add(0, new ExitStrategy());
            strategies.Add(1, new ShowBooksInLibraryStrategy());
            strategies.Add(2, new ShowAllDepartmentBookStrategy());
            strategies.Add(3, new ShowAllAutorBookStrategy());
            strategies.Add(4, new AddBookToDepartmentStrategy());
            strategies.Add(5, new SearchBookByNameStartegy());
            strategies.Add(6, new ShowBookWithTheFewestPagesStrategy());
            strategies.Add(7, new ShowBookWithTheFewestPagesStrategy());
            strategies.Add(8, new SortBookByNameStrategy());
            strategies.Add(9, new WriteLibraryToLibraryXmlStrategy());
            strategies.Add(10, new AddDepartmentToLibraryStrategy());
            do
            {
                Console.WriteLine("Library\n");
                Console.WriteLine("\n1.Show all books in library.\n");
                Console.WriteLine("2.Show all books in department.\n");
                Console.WriteLine("3.Show all autor books.\n");
                Console.WriteLine("4.Add book to department.\n");
                Console.WriteLine("5.Search book in library by name.\n");
                Console.WriteLine("6.Show book with the fewest pages.\n");
                Console.WriteLine("7.Show author with the most books.\n");
                Console.WriteLine("8.Sort all book in library by name.\n");
                Console.WriteLine("9.Write library to Library.xml.\n");
                Console.WriteLine("10.Add department to Library\n");
                Console.WriteLine("0.Exit.\n");
                choice = Int32.Parse(Console.ReadLine());
                if (choice >= 0 && choice <= strategies.Count)
                {
                    menuStrategy = strategies[choice];
                    menuStrategy.DoSomething(library);
                }
                else
                {
                    Console.Clear();
                }
            } while (choice != 0);

            Console.ReadLine();
        }

        public static void AddAutorsToProgram(Library library)
        {
            var gerbertShildt = new Autor("Gerbert", "Shildt");
            library.AllAutors.Add(gerbertShildt);
            var joanneRowling = new Autor("Joanne", "Rowling");
            library.AllAutors.Add(joanneRowling);
            var tarasShevchenko = new Autor("Taras", "Shevchenko");
            library.AllAutors.Add(tarasShevchenko);
        }
    }
}
