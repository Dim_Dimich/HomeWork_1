﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class UkrainianLiteratureDepartment : Department
    {
        public UkrainianLiteratureDepartment() : this("") { }

        public UkrainianLiteratureDepartment(string name) : base(name) {
            
        }
    }
}
