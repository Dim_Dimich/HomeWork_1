﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Autor : BaseEntity, ICountingBooks
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<Book> AllAutorBooks = new List<Book>();
        public Autor() : this("", "") { }
        public Autor(string fName, string lName)
        {
            FirstName = fName;
            LastName = lName;
        }

        public void CountOfBooks()
        {
            Console.WriteLine(string.Format("This autor write {0} books.", this.AllAutorBooks.Count));
        }

        public void AddBookToAutorColection(Book book)
        {
            AllAutorBooks.Add(book);
            Console.WriteLine("Book was successfully added");
        }

        public void ShowAllBooksForAutor()
        {
            foreach (var book in this.AllAutorBooks)
            {
                Console.WriteLine("////////////");
                Console.WriteLine(book.ToString());
            }
        }
    }
}
