﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Book : BaseEntity
    {
        private string _name;
        private int _pageCount;

        public Autor autor;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public int PageCount
        {
            get
            {
                return _pageCount;
            }
            set
            {
                _pageCount = value;
            }
        }
        public Book() : this("", int.MaxValue, new Autor()) { }

        public Book(string bName, int pCount, Autor bookAutor)
        {
            Name = bName;
            PageCount = pCount;
            autor = bookAutor;
        }

        public override string ToString()
        {
            return string.Format("Book name : {0}\nAutor : {1} {2}\nPages : {3}", Name, autor.FirstName, autor.LastName, PageCount);
        }
    }
}
