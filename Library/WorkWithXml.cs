﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Library
{
    public class WorkWithXml
    {
        public void WriteInXmlDocument(Library libarry)
        {
            XElement Library = new XElement("BooksLibrary");
            foreach (var department in libarry.DepartmentList)
            {
                XElement departmentXElement = new XElement("department");
                departmentXElement.SetAttributeValue("name", department.Name);
                foreach (var book in department.AllBookInDepartment)
                {
                    XElement bookXelement = new XElement("book");
                    bookXelement.SetAttributeValue("name", book.Name);
                    bookXelement.SetAttributeValue("pages", book.PageCount);
                    bookXelement.SetAttributeValue("autor", book.autor.FirstName + " " + book.autor.LastName);
                    departmentXElement.Add(bookXelement);
                }
                Library.Add(departmentXElement);
            }
            XDocument xdoc = new XDocument(Library);
            xdoc.Save("Library2.xml");
            Console.WriteLine("Library successfully written to the file Library2.xml.\n");
        }

        public void ReadFromXmlDocument(Library library)
        {
            XDocument xDoc = XDocument.Load("Library.xml");
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement bookLibraryXel = xDoc.Element("BooksLibrary");
                if (bookLibraryXel != null && bookLibraryXel.HasElements)
                {
                    foreach (var department in bookLibraryXel.Elements("department"))
                    {
                        var readedDepartment = new Department(department.Attribute("name").Value);
                        library.AddDepartmentToLibarry(readedDepartment);
                        foreach (var book in department.Elements("book"))
                        {
                            var newBook = new Book();
                            newBook.Name = book.Attribute("name").Value;
                            newBook.PageCount = Int32.Parse(book.Attribute("pages").Value);
                            foreach (var autor in library.AllAutors)
                            {
                                if ((autor.FirstName + " " + autor.LastName) == book.Attribute("autor").Value)
                                {
                                    newBook.autor = autor;
                                    break;
                                }
                            }
                            if (readedDepartment != null)
                            {
                                readedDepartment.AddBookToDepartment(newBook);
                            }
                        }
                    }
                }
            }
        }
    }
}
