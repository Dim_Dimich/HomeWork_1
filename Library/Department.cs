﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Department : BaseEntity, IComparable, ICountingBooks
    {
        public string Name { get; set; }
        public List<Book> AllBookInDepartment = new List<Book>();

        public int CompareTo(object obj)
        {
            Department department = obj as Department;
            if (obj != null)
            {
                return this.AllBookInDepartment.Count.CompareTo(department.AllBookInDepartment.Count);
            }

            throw new Exception("It's impossible to compare two objects");
        }
        public Department() : this("") { }

        public Department(string dName)
        {
            Name = dName;
        }

        public void CountOfBooks()
        {
            Console.WriteLine(string.Format("There are {0} in this department", AllBookInDepartment.Count));
        }

        public void ShowAllBookInDepartment()
        {
            if (this.AllBookInDepartment.Count <= 0)
            {
                Console.WriteLine("There are no books in this department.\n");
                return;
            }

            foreach (var book in this.AllBookInDepartment)
            {
                Console.WriteLine("////////////");
                Console.WriteLine(book.ToString() + "\n");
            }
        }

        public void AddBookToDepartment(Book book)
        {
            this.AllBookInDepartment.Add(book);
            Console.WriteLine("Book was successfully added");
        }

        public override string ToString()
        {
            return string.Format("Department name : {0}", Name);
        }
    }
}
