﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Library : BaseEntity, ICountingBooks
    {
        public List<Department> DepartmentList = new List<Department>();
        //public List<Autor> AllAutors { get; set; }
        public List<Autor> AllAutors = new List<Autor>();

        public void CountOfBooks()
        {
            int countOfAllBooksInLibrary = 0;
            Department biggest = DepartmentList[0];
            foreach (var department in DepartmentList)
            {
                if (department.AllBookInDepartment.Count > biggest.AllBookInDepartment.Count)
                {
                    biggest = department;
                }
                countOfAllBooksInLibrary += department.AllBookInDepartment.Count;
            }
            if (countOfAllBooksInLibrary == 0)
            {
                Console.WriteLine("Library doesn't have books yet.\n");
            }
            else
            {
                Console.WriteLine(string.Format("There are {0} book in library.\n", countOfAllBooksInLibrary));
                Console.WriteLine("{0} has the largest number of books.\n", biggest.Name);
                ShowAllBooksInLibarry();
            }
        }

        public Autor ChoiceAutor()
        {
            int choice = -1;
            do
            {
                Console.WriteLine("Choice autor number: \n");
                for (int i = 0; i < AllAutors.Count; i++)
                {
                    Console.WriteLine((i + 1) + "." + AllAutors[i].FirstName + " " + AllAutors[i].LastName);
                }
                choice = Int32.Parse(Console.ReadLine());
            } while (choice > 0);

            return AllAutors[choice];
        }
        public void AddBookToDepartment(Department department)
        {
            int choice = 0;
            var book = new Book();
            Console.WriteLine("Enter book name : ");
            book.Name = Console.ReadLine();
            Console.WriteLine("Enter book page count : ");
            book.PageCount = Int32.Parse(Console.ReadLine());
            Console.Clear();
            do
            {
                Console.WriteLine("Autor");
                if (AllAutors.Count > 0)
                {
                    Console.WriteLine("1.Add autor.\n");
                    Console.WriteLine("2.Choose from existing autors.\n");
                }
                else
                {
                    Console.WriteLine("1.Add autor.\n");
                }
                choice = Int32.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        book.autor = AddAutor();
                        break;
                    case 2:
                        Console.Clear();
                        book.autor = ChoiceAutor();
                        break;
                    default:
                        Console.WriteLine("Only 1 or 2 number!!!");
                        break;
                }
            } while (choice < 1 || choice > 2);

            department.AddBookToDepartment(book);
        }

        public Department ChoiceDepartment()
        {
            int choice = -1;
            do
            {
                Console.WriteLine("Choice department number: \n");
                for (int i = 0; i < DepartmentList.Count; i++)
                {
                    Console.WriteLine((i + 1) + "." + DepartmentList[i].Name + "\n");
                }
                choice = Int32.Parse(Console.ReadLine());
            } while (choice < 0 || choice > DepartmentList.Count || choice == 0);
            return DepartmentList[choice - 1];
        }

        public void AddDepartmentToLibrary()
        {
            var department = new Department();
            Console.WriteLine("Enter name of department: ");
            department.Name = Console.ReadLine();
            if (department.Name != null)
            {
                DepartmentList.Add(department);
                Console.WriteLine("Department was successfully added.\n");
            }
        }

        public void AddDepartmentToLibarry(Department department)
        {
            DepartmentList.Add(department);
            Console.WriteLine("Department was successfully added.\n");
        }

        public Autor AddAutor()
        {
            var autor = new Autor();
            Console.WriteLine("Enter autor first name : \n");
            autor.FirstName = Console.ReadLine();
            Console.WriteLine("Enter autor last name : \n");
            autor.LastName = Console.ReadLine();
            Console.WriteLine("Autor was successfully added.\n");
            AllAutors.Add(autor);
            return autor;
        }

        public void SortBooksByName()
        {
            var sortedList = new List<Book>();
            foreach (var department in DepartmentList)
            {
                foreach (var book in department.AllBookInDepartment)
                {
                    sortedList.Add(book);
                }
            }
            if (sortedList.Count != 0)
            {
                sortedList = sortedList.OrderBy(x => x.Name).ToList();
                ShowAllBooksInLibarry();
            }
            else
            {
                Console.WriteLine("Library doesn't have books\n");
                return;
            }
        }

        public void ShowAllBooksInLibarry()
        {
            foreach (var department in DepartmentList)
            {
                foreach (var book in department.AllBookInDepartment)
                {
                    Console.WriteLine("////////////");
                    Console.WriteLine(book.ToString() + "\n");
                }
            }
        }

        public void SearchBookByName(string name)
        {
            Book book = null;
            foreach (var department in this.DepartmentList)
            {
                book = department.AllBookInDepartment.Find(x => x.Name == name);
            }
            if (book == null)
            {
                Console.WriteLine("No such book in the library.\n");
            }
            else
            {
                Console.WriteLine("Your book is : \n");
                Console.WriteLine("////////////");
                Console.WriteLine(book.ToString());
            }
        }

        public void ShowBookWithFewestPages()
        {
            var book = new Book();
            foreach (var department in this.DepartmentList)
            {
                for (int i = 0; i < department.AllBookInDepartment.Count; i++)
                {
                    if (department.AllBookInDepartment[i].PageCount < book.PageCount)
                    {
                        book = department.AllBookInDepartment[i];
                    }
                }
            }
            Console.WriteLine("A smallest book is : " + book.Name);
        }

        public void ShowAutorWithTheMostBooks()
        {
            Autor autor = this.AllAutors[0];
            foreach (var newAutor in this.AllAutors)
            {
                if (newAutor.AllAutorBooks.Count > autor.AllAutorBooks.Count)
                {
                    autor = newAutor;
                }
            }
            Console.WriteLine(string.Format("Autro with the most books is {0} {1}", autor.FirstName, autor.LastName));
        }
    }
}
