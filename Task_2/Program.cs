﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter m: ");
            int m = Int32.Parse(Console.ReadLine());
            Console.Write("Enter m: ");
            int n = Int32.Parse(Console.ReadLine());
            int count = 0;
            int[] mas = new int[m];
            int[,] matrix = new int[m, n];
            int number = -1;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.WriteLine("[" + (i + 1) + "][" + (j + 1) + "] = ");
                    matrix[i, j] = Int32.Parse(Console.ReadLine());
                }
            }
            for (int j = 0; j < n; j++)
            {
                for (int i = 0; i < m; i++)
                {
                    if (matrix[i, j] < 0)
                    {
                        number = -1;
                        break;
                    }
                    else
                    {
                        number = j + 1;
                    }
                }
                if (number > -1)
                {
                    Console.WriteLine("Column number : " + number);
                    break;
                }
                else if (j == n - 1 && number == -1)
                {
                    Console.WriteLine("All colums include negative number");
                }
            }
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    for (int k = m - 1; k >= j; k--)
                    {
                        if (matrix[i, j] == matrix[i, k])
                        {
                            count++;
                        }
                        if (k - 1 == j)
                        {
                            if (count > 0)
                            {
                                mas[i] = count + 1;
                                count = 0;
                            }
                            else
                            {
                                mas[i] = count;
                                count = 0;
                            }
                        }
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
